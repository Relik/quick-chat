// var SocketIo = require("socket.io");
const db = require("../database/mongo-db");

class SocketManager {
  constructor(server) {
    this.socket = require("socket.io")(server);

    console.log("start socket");
    this.socket.on("connection", user => {
      console.log("New user connected ");

      user.on("sendMessage", msg => {
        console.log("New message ", msg);
        db.saveMessage(msg).then(newMsg => {
          this.sendNewestMsg(newMsg);
        });
      });
      user.on("getMessages", () => {
        this.sendChatInfo(user);
      });
      user.on("deleteMessage", id => {
        db.deleteMessage(id).then(res=>{
          this.sendChatInfo(this.socket);
        }).catch(err=>{
          console.log(err);
        });
      });
    });
  }
  sendChatInfo(target) {
    db.getMessages().then(messages => {
      target.emit("messages", messages);
    });
  }
  sendNewestMsg(msg) {
    this.socket.emit("newMessage", msg);
  }
}

exports.SocketManager = SocketManager;
