const path = require("path");

exports.mainSite = function(req, res) {
  console.log(
    path.join(
      path.dirname(require.main.filename) + "/dist/quick-chat/index.html"
    )
  );
  res.sendFile(
    path.join(
      path.dirname(require.main.filename) + "/dist/quick-chat/index.html"
    )
  );
};
