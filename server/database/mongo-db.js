// 2s2Zjl1PUCKUGBqO
var schemas = require("./shemas");
var mongoose = require("mongoose");

var DB_CONNECTION =
  "mongodb+srv://admin:2s2Zjl1PUCKUGBqO@quick-chat.k6nnm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
// MongoClient.connect(connectionString, (err, client) => {
//   if (err) return console.error(err);
//   console.log("Connected to Database");
// });
const USER_COLLECTION = "users";
const MESSAGES_COLLECTION = "messages";
const User = mongoose.model("User", schemas.userSchema, USER_COLLECTION);
const Message = mongoose.model(
  "Message",
  schemas.messageSchema,
  MESSAGES_COLLECTION
);

var mongooseOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};
mongoose
  .connect(DB_CONNECTION, mongooseOptions, function() {})
  .then(res => {
    console.log("Connected to database");
  })
  .catch(err => {
    console.log("App starting error:", err.stack);
    process.exit(1);
  });

function saveUser(data) {
  let newUser = new User(data);
  return newUser.save().then(res => {
    console.log("Added new user: ", res);
    return res;
  });
}
function saveMessage(data) {
  let newMessage = new Message(data);
  return newMessage
    .save()
    .then(res => {
      console.log("Added new message: ", res);
      return res;
    })
    .catch(err => {
      return new Error(err);
    });
}
function getMessages() {
  return Message.find()
    .then(res => {
      return res;
    })
    .catch(err => {
      return new Error(err);
    });
}
function deleteMessage(id) {
  console.log('Usuwam obiekt o id: ', id);
  return Message.deleteOne({ _id: id })
    .then(res => {
      return res;
    })
    .catch(err => {
      return new Error(err);
    });
}

module.exports = {
  saveUser: saveUser,
  saveMessage: saveMessage,
  getMessages: getMessages,
  deleteMessage: deleteMessage
};
