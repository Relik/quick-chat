var mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  nick: {type: String, required: true, unique: false}
});

const messageSchema = new mongoose.Schema({
  id: mongoose.Schema.Types.ObjectId,
  authorNick: String,
  authorId: String,
  date: Number,
  content: String
});

exports.userSchema = userSchema;
exports.messageSchema = messageSchema;
