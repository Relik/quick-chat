const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const app = express();
const path = require("path");

var rootRouter = require("./root-router").rootRouter;
var authRouter = require("./auth-router").authRouter;

var whitelist = [
  "https://quick-chat-0.herokuapp.com",
  "http://localhost:4200",
  "http://localhost:3001"
];
var corsOptions = {
  origin: function(origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      console.log("notallowed");
      callback(new Error(`Not allowed by CORS origin: ${origin}`));
    }
  }
};
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(
  express.static(
    path.join(path.dirname(require.main.filename) + "/dist/quick-chat/")
  )
);
app.use("/auth", authRouter);
app.use("/", rootRouter);
exports.app = app;
