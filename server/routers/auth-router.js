var router = require('express').Router();
var authController = require('../controllers/auth-controller');

router.post('/login', authController.login);

exports.authRouter = router;