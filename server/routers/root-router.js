var router = require('express').Router();
var rootController = require('../controllers/root-controller');

router.get('/', rootController.mainSite);
// router.get('/auth/xd', ()=>console.log('xd'));
router.get('*/*', rootController.mainSite);
exports.rootRouter = router;
