import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthUserService, Status } from '../auth/auth-user.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators'


@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.less']
})
export class WelcomeScreenComponent implements OnInit, OnDestroy {
  nick: string;
  status: Status;
  Status = Status;
  constructor(private authService: AuthUserService, private router: Router) { }

  ngOnInit() {
    this.status = this.authService.status;
    this.nick = this.status === Status.LoggedIn ? this.authService.currentUser.nick : '';
  }
  ngOnDestroy() {

  }
  chatNavigate(){
    this.router.navigate(['chat']);
  }
  login(){
    this.status = Status.Loading;
    this.authService.signup(this.nick).pipe(first()).subscribe((data)=>{
      this.router.navigate(['chat'])
    }, (error)=>{
      console.log("Error when logging in: ", error)
      this.status = Status.LoggedOut;
  
    })
  }
  logout(){
    this.authService.logout();
    this.status = this.authService.status;
    this.nick = '';
  }
}
