import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {
  transform(value: string, extend: boolean = false): string {
    if (!extend && value.length >= 200) 
      return value.substr(0, 200) + '... click to expand';
    else 
      return value;
  }
}
