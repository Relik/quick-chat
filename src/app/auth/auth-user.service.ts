import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

interface AuthResponseData {
  nick: string;
  id: string;
  // expiresIn: number;
}

export enum Status {
  LoggedOut = 0,
  LoggedIn,
  Loading
}

@Injectable({
  providedIn: 'root'
})
export class AuthUserService {
  status: Status = Status.LoggedOut;
  currentUser: AuthResponseData;
  constructor(private http: HttpClient) {}

  signup(nick: string): Observable<AuthResponseData> {
    this.status = Status.Loading;
    // return Observable.create(observer => {
    //   setTimeout(() => {
    //     this.status = Status.LoggedIn;
    //     this.currentUser = {
    //       nick: nick,
    //       token: '1',
    //       expiresIn: Date.now() + 10000
    //     };
    //     localStorage.setItem('quickChatUserData', JSON.stringify(this.currentUser))
    //     observer.next(this.currentUser);
    //   }, 800);
    // });
    return this.http
      .post<AuthResponseData>(`${environment.apiUrl}/auth/login`, {
        nick: nick
      })
      .pipe(
        tap(resData => {
          this.currentUser = resData;
          localStorage.setItem(
            'quickChatUserData',
            JSON.stringify(this.currentUser)
          );
          this.status = Status.LoggedIn;
        })
      );
  }
  autoLogin() {
    let userData: AuthResponseData = JSON.parse(
      localStorage.getItem('quickChatUserData')
    );
    if (userData) {
      this.currentUser = userData;
      this.status = Status.LoggedIn;
    }
  }
  logout() {
    this.status = Status.LoggedOut;
    this.currentUser = null;
    localStorage.removeItem('quickChatUserData');
  }
}
