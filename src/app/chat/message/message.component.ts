import { Component, OnInit, Input } from '@angular/core';
import { Message, ChatService } from '../chat.service';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.less']
})
export class MessageComponent implements OnInit {

  @Input('message')
  message: Message;
  @Input('isMine')
  isMine: boolean;
  isExpanded: boolean;

  constructor(private chatService: ChatService) { }

  ngOnInit(): void {
  }

  deleteMsg(){
    //@TODO: Alert box
    this.chatService.deleteMessage(this.message._id)
  }


}
