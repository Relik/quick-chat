import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy,
  AfterViewChecked
} from '@angular/core';
import { ChatService, Message } from './chat.service';
import { AuthUserService } from '../auth/auth-user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less']
})
export class ChatComponent implements OnInit, OnDestroy, AfterViewChecked {
  messages: Array<Message>;
  messagesSub: Subscription;
  newMessageSub: Subscription;
  constructor(
    private chatService: ChatService,
    private renderer: Renderer2,
    private authService: AuthUserService
  ) {}
  gotNewMessage: boolean = false;
  userId: string;
  @ViewChild('text')
  currentMsg: ElementRef;
  @ViewChild('lastMessage')
  lastMsg: ElementRef;
  ngOnInit(): void {
    this.messagesSub = this.chatService.messages.subscribe(msgs => {
      this.messages = msgs;
      this.gotNewMessage = true;
    });
    this.newMessageSub = this.chatService.newestMsg.subscribe(msg => {
      this.messages.push(msg);
      this.gotNewMessage = true;
    });
    this.chatService.getMessages();
    this.userId = this.authService.currentUser.id;
  }
  ngAfterViewChecked() {
    if (this.gotNewMessage) {
      this.lastMsg.nativeElement.scrollIntoView();
      this.gotNewMessage = false;
    }
  }
  ngOnDestroy() {
    this.messagesSub.unsubscribe();
  }
  sendMessage() {
    this.chatService.sendMessage({
      authorId: this.authService.currentUser.id,
      content: this.currentMsg.nativeElement.value,
      date: Date.now(),
      authorNick: this.authService.currentUser.nick
    });
    this.renderer.setProperty(this.currentMsg.nativeElement, 'value', '');
  }
}
