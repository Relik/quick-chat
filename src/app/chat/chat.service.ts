import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Socket } from 'ngx-socket-io';

export interface Message {
  _id?: string,
  date: number;
  authorNick: string;
  content: string;
  authorId: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private socket: Socket) {}
  messages = this.socket.fromEvent<Message[]>('messages');
  newestMsg = this.socket.fromEvent<Message>('newMessage');
  socketSubject: Subject<MessageEvent>;

  sendMessage(msg: Message) {
    this.socket.emit('sendMessage', msg);
  }
  getMessages() {
    this.socket.emit('getMessages');
  }
  deleteMessage(id) {
    this.socket.emit('deleteMessage', id)
  }
}
