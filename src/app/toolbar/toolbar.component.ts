import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent implements OnInit {

  ngOnInit(): void {
  }
  constructor(private router: Router){

  }
  navigate(target: string){
    this.router.navigate([target]);
  }
}
