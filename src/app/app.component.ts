import { Component, OnInit } from '@angular/core';
import { AuthUserService } from './auth/auth-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{
  title = 'quick-chat';

  constructor(private authService: AuthUserService){}

  ngOnInit() {
    this.authService.autoLogin();
  }
}
