// const express = require('express');
// const app = express();
// const path = require('path');
// const port = process.env.PORT || 3000;
// app.use(express.static(path.join(__dirname + '/dist/quick-chat/')));
// // app.server.get('*.*', express.static(__dirname, {maxAge: '1y'}));

// app.get('/', function(req, res) {
//     res.sendFile(path.join(__dirname + '/dist/quick-chat/index.html'));
// });

// app.listen(port, () => console.log(`url-shortener listening on port ${port}!`));



// require('dotenv').load();
var app = require('./server/routers/index').app;
var server = require('http').createServer(app);
const Socket = require('./server/socket/socket-manager').SocketManager;
socket = new Socket(server);

// ioSocket = require('./socket/socketEventSubscriber').ioSocket(server);
// logger = require('./classes/logger/logger').Logger;

const PORT = process.env.PORT || 3001;

server.listen(PORT, () => {
    console.log(`Hello, I\'m listening on PORT:${PORT}`)
});